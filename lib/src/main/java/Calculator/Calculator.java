package Calculator;


import PublicTools.Utility;

public class Calculator {
    private int _number1, _number2;
    private String _oprator;

    public Calculator(int number1, int number2, String oprator) {
        _number1 = number1;
        _number2 = number2;
        _oprator = oprator;
    }

    public void calculate() {
        int result = 0;
        switch (_oprator) {
            case "+":
                result = _number1 + _number2;
                break;
            case "-":
                result = _number1 - _number2;
                break;
            case "*":
                result = _number1 * _number2;
                break;
            case "/":
                result = _number1 / _number2;
                break;
                default:
                    result=0;
        }
        Utility.showMessageLn(String.format(" %d  %s  %d  =  %d",_number1,_oprator,_number2,result));
    }

}
