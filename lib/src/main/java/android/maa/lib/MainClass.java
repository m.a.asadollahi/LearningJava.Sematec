package android.maa.lib;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import Calculator.Calculator;
import PublicTools.Utility;
import Register.RegisterActivity;
import Register.Student;

public class MainClass {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        int studentCode = 0;
        String userChoos;
        List<Student> studentList = new ArrayList<>();
        Utility.showMessageLn("Welcome To My App ...");
        Utility.showMessageLn("Choose : ");
        Utility.showMessageLn("Calculator (Perss Captal C)");
        Utility.showMessageLn("Student Register (Perss Captal R)");
        userChoos = scanner.nextLine();
        Utility.showMessageLn(userChoos);
        if (userChoos.equals("R")) {
            Utility.showMessageLn("Please enter student count for register : ");
            studentCode = scanner.nextInt();
            if (studentCode < 0 || studentCode == 0) {
                Utility.showMessageLn("Ooops Worng Count ..... goodbye");
            } else {
                studentList = RegisterActivity.getStudentData(studentCode);
                Utility.showMessageLn("-----------------------------------");
                RegisterActivity.showStudents(studentList);
            }

        } else if (userChoos.equals("C")) {
            int num1, num2;
            String opr;
            Utility.showMessage("Please enter number 1 : ");
            num1 = scanner.nextInt();
            Utility.showMessage("Please enter oprator : ");
            opr = scanner.next();
            Utility.showMessage("Please enter number 2 : ");
            num2 = scanner.nextInt();
            Calculator calculator = new Calculator(num1, num2, opr);
            calculator.calculate();
        } else
            Utility.showMessageLn("Ooops Worng Count ..... goodbye");

    }
}
