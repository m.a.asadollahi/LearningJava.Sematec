package Register;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import PublicTools.Utility;

public class RegisterActivity {
    public static Scanner scanner = new Scanner(System.in);

    public static List<Student> getStudentData(int studentCount) {
        List<Student> studentList = new ArrayList<>();
        Student newStudent;
        for (int i = 0; i < studentCount; i++) {
            newStudent = new Student();
            Utility.showMessage(String.format("Please Enter Name Student ( %d ) : ", (i + 1)));
            newStudent.setName(scanner.nextLine());
            Utility.showMessage(String.format("Please Enter LastName Student ( %d ) : ", (i + 1)));
            newStudent.setLastName(scanner.nextLine());
            Utility.showMessage(String.format("Please Enter NationalID Student ( %d ) : ", (i + 1)));
            newStudent.setNationalID(scanner.nextLine());
            Utility.showMessage(String.format("Please Enter Mobile Number ( %d ) : ", (i + 1)));
            newStudent.setMobile(scanner.nextLine());
            Utility.showMessage(String.format("Please Enter StudentCode Student ( %d ) : ", (i + 1)));
            newStudent.setStudentCode(scanner.nextLine());
            Utility.showMessage(String.format("Please Enter Enter Year Student ( %d ) : ", (i + 1)));
            newStudent.setEnterYear(scanner.nextLine());
            Utility.showMessageLn("-----------------------------------");
            studentList.add(newStudent);
        }
        return studentList;
    }

    public static void showStudent(Student currentStudent) {
        Utility.showMessageLn("Student : " + currentStudent.getName() + " " + currentStudent.getLastName() + " {");
        Utility.showMessageLn("           Code = " + currentStudent.getStudentCode());
        Utility.showMessageLn("           NationalID = " + currentStudent.getNationalID());
        Utility.showMessageLn("           Mobile Number = " + currentStudent.getMobile());
        Utility.showMessageLn("           Enter Year = " + currentStudent.getEnterYear() + "  } ");
        Utility.showMessageLn("****************************************************************");
    }

    public static void showStudents(List<Student> studentList) {

        for (Student currentStudent : studentList
                ) {
            showStudent(currentStudent);
        }
    }
}
